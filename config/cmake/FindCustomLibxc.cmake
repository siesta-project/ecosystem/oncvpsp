#
find_package(PkgConfig REQUIRED QUIET)


if(NOT "$ENV{LIBXC_ROOT}" STREQUAL "")
  message(STATUS "Using LIBXC_ROOT as a hint for pkg-config: $ENV{LIBXC_ROOT}")

   list(PREPEND CMAKE_PREFIX_PATH "$ENV{LIBXC_ROOT}")
endif()

message(STATUS "Searching for libXC library")

# This will be a required module
pkg_check_modules(LIBXC_C libxc)

if($ENV{LIBXC_ROOT})
 list(POP_FRONT CMAKE_PREFIX_PATH)
endif()

if (NOT LIBXC_C_FOUND)
  message(STATUS "Libxc not found. Try to specify LIBXC_ROOT")
  message(FATAL_ERROR "Libxc not found")
endif()

message(DEBUG "LIBXC_C_LIBDIR: ${LIBXC_C_LIBDIR}")
message(DEBUG "LIBXC_C_LIBRARIES: ${LIBXC_C_LIBRARIES}")
message(DEBUG "LIBXC_C_LINK_LIBRARIES: ${LIBXC_C_LINK_LIBRARIES}")
message(DEBUG "LIBXC_C_INCLUDEDIR: ${LIBXC_C_INCLUDEDIR}")
message(DEBUG "LIBXC_C_INCLUDE_DIRS: ${LIBXC_C_INCLUDE_DIRS}")

 string(REPLACE "." ";" VERSION_LIST "${LIBXC_C_VERSION}")
 
 list(GET VERSION_LIST 0 LIBXC_VERSION_MAJOR)
 list(GET VERSION_LIST 1 LIBXC_VERSION_MINOR)
 list(GET VERSION_LIST 2 LIBXC_VERSION_PATCH)

 math(EXPR LIBXC_VERSION " 100 * ${LIBXC_VERSION_MAJOR}
                          + 10 * ${LIBXC_VERSION_MINOR}
			  +      ${LIBXC_VERSION_PATCH}" OUTPUT_FORMAT DECIMAL)
			 
 message(STATUS "LIBXC_VERSION: ${LIBXC_VERSION}")

 # Check version number
 if (NOT "${LIBXC_VERSION_MAJOR_MAX}" STREQUAL "")
  if (${LIBXC_VERSION_MAJOR} GREATER "${LIBXC_VERSION_MAJOR_MAX}" )
    message(STATUS
    " ** The Libxc version (${LIBXC_C_VERSION}) is too high. Can use up to ${LIBXC_VERSION_MAJOR_MAX}.X")
    message(FATAL_ERROR "Libxc version too high")
  endif()
 endif()

 if (NOT "${LIBXC_VERSION_MAJOR_MIN}" STREQUAL "")
  if (${LIBXC_VERSION_MAJOR} LESS "${LIBXC_VERSION_MAJOR_MIN}")
    message(STATUS
      "** The Libxc version (${LIBXC_C_VERSION}) is too low. Needs at least ${LIBXC_VERSION_MAJOR_MIN}.X")
    message(FATAL_ERROR "Libxc version too low")
  endif()
 endif()
 
if(NOT TARGET Libxc::xc)
  # we need to add the target if it does not exist
  add_library(Libxc::xc INTERFACE IMPORTED)

  target_link_libraries(
    Libxc::xc
    INTERFACE "${LIBXC_C_LINK_LIBRARIES}"
  )
  target_include_directories(
    Libxc::xc
    INTERFACE "${LIBXC_C_INCLUDE_DIRS}" "${LIBXC_C_INCLUDEDIR}"
  )
endif()

# Get the directory in which the C libxc component was found...
get_filename_component(libxc_PREFIX "${LIBXC_C_LIBDIR}" DIRECTORY)
message(STATUS "libxc_PREFIX: ${libxc_PREFIX}")

# ... and make it the only place in which the Fortran pieces will be searched for

set(avoided_heuristics
    NO_CMAKE_PATH
    NO_DEFAULT_PATH
    NO_CMAKE_ENVIRONMENT_PATH
    NO_SYSTEM_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    )

find_path(LIBXC_F90_INCLUDEDIR
  ${avoided_heuristics}
  NAMES xc_f90_types_m.mod   # ** CHECK
  PATH_SUFFIXES include
  HINTS
  ${libxc_PREFIX})

message(STATUS "LIBXC_F90_INCLUDEDIR: ${LIBXC_F90_INCLUDEDIR}")

find_library(LIBXC_F90_LINK_LIBRARIES
  ${avoided_heuristics}
  NAMES xcf90
  PATH_SUFFIXES lib lib64
  HINTS
  ${libxc_PREFIX}
  DOC "libxc F90 library")

message(STATUS "LIBXC_F90_LINK_LIBRARIES: ${LIBXC_F90_LINK_LIBRARIES}")

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(CustomLibxc "DEFAULT_MSG"
                                  LIBXC_C_LINK_LIBRARIES LIBXC_F90_LINK_LIBRARIES
                                  LIBXC_C_INCLUDEDIR LIBXC_F90_INCLUDEDIR)

if(CustomLibxc_FOUND AND NOT TARGET Libxc::xc_Fortran)

    add_library(Libxc::xcf90 INTERFACE IMPORTED)

    target_link_libraries(
      Libxc::xcf90
      INTERFACE "${LIBXC_F90_LINK_LIBRARIES}"
    )

    target_include_directories(
      Libxc::xcf90
      INTERFACE "${LIBXC_F90_INCLUDEDIR}"
    )
  
    add_library(Libxc::xc_Fortran INTERFACE IMPORTED)
    target_link_libraries(Libxc::xc_Fortran INTERFACE Libxc::xcf90 Libxc::xc)

endif()


