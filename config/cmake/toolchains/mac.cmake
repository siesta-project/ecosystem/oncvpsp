#
#  If you have veclibfort, and it works, use this line
#
set(BLAS_LIBRARIES "-lveclibfort" CACHE STRING "blas library chosen")
#
# or maybe set
#
# set(CMAKE_Fortran_FLAGS "$ENV{FFLAGS} -lveclibfort" CACHE STRING "compiler flags for sane lapack")
#
# (In the above case the LAPACK libraries will be reported as "implicitly linked").
#
#  More general settings for use with shell modules
#
# set(LAPACK_LIBRARY "$ENV{LAPACK_LIBS}" CACHE STRING "lapack library chosen")
#
# These can be overridden from the command line
#
#set(CMAKE_BUILD_TYPE "Debug" CACHE STRING "build_type")
#set(Fortran_FLAGS_DEBUG  "-g -O0" CACHE STRING "Fortran debug flags")

