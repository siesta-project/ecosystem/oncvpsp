set(srcs

  aeo.f90
  check_data.f90
  const_basis.f90
  der2exc.f90
  dpnint.f90
  eresid.f90
  exc_off_pbe.f
  exchdl.f90
  excpzca.f90
  excwig.f90
  fphsft.f90
  fphsft_r.f90
  fpovlp.f90
  fpovlp_r.f90
  gg1cc.f90
  gnu_script.f90
  gnu_script_r.f90
  gp1cc.f90
  gpp1cc.f90
  ldiracfb.f90
  ldiracfs.f90
  linout.f90
  linout_r.f90
  lschfb.f90
  lschfs.f90
  lschkb.f90
  lschpb.f90
  lschps.f90
  lschpsbar.f90
  lschpse.f90
  lschvkbb.f90
  lschvkbbe.f90
  lschvkbs.f90
  m_getopts.f90
  m_psmlout.f90
  m_uuid.f90
  modcore.f90
  modcore2.f90
  modcore3.f90
  old_optimize.f90
  optimize.f90
  psatom.f90
  psatom_r.f90
  pspot.f90
  qroots.f90
  relatom.f90
  renorm_r.f90
  run_config.f90
  run_config_r.f90
  run_diag.f90
  run_diag_r.f90
  run_diag_sr_so_r.f90
  run_ghosts.f90
  run_optimize.f90
  run_phsft.f90
  run_phsft_r.f90
  run_plot.f90
  run_plot_r.f90
  run_vkb.f90
  run_vkb_r.f90
  sbf8.f90
  sbf_basis.f90
  sbf_rc_der.f90
  sr_so_r.f90
  sratom.f90
  tfapot.f90
  upfout.f90
  upfout_r.f90
  vkboutwf.f90
  vkbphsft.f90
  vout.f90
  vpinteg.f90
  vploc.f90
  wellstate.f90
  wellstate_r.f90
  wf_rc_der.f90
)

if(WITH_LIBXC)
  set(srcs ${srcs} functionals.F90  exc_libxc.f90)
  
  set_source_files_properties(
    functionals.F90
    PROPERTIES
    COMPILE_DEFINITIONS LIBXC_VERSION=${LIBXC_VERSION}
  )

else()
  set(srcs ${srcs}    exc_libxc_stub.f90)
endif()

  
add_library(oncv-lib ${srcs})

add_executable(oncvpsp  oncvpsp.f90)
add_executable(oncvpsp_r  oncvpsp_r.f90)
add_executable(oncvpsp_nr  oncvpsp_nr.f90)

target_link_libraries(oncv-lib
		      $<$<BOOL:${WITH_LIBXC}>:Libxc::xc_Fortran>
                      xmlf90::xmlf90)

target_link_libraries(oncvpsp
                      oncv-lib
		      $<$<BOOL:${WITH_LIBXC}>:Libxc::xc_Fortran>
		      xmlf90::xmlf90
		      LAPACK::LAPACK
		      
)
		      
target_link_libraries(oncvpsp_r
                      oncv-lib
     		      $<$<BOOL:${WITH_LIBXC}>:Libxc::xc_Fortran>
                      xmlf90::xmlf90 LAPACK::LAPACK)

target_link_libraries(oncvpsp_nr
                      oncv-lib
      		      $<$<BOOL:${WITH_LIBXC}>:Libxc::xc_Fortran>
                      xmlf90::xmlf90 LAPACK::LAPACK)

set_target_properties(
  oncvpsp
  PROPERTIES
  OUTPUT_NAME "oncvpsp.x"
)
set_target_properties(
  oncvpsp_r
  PROPERTIES
  OUTPUT_NAME "oncvpspr.x"
)
set_target_properties(
  oncvpsp_nr
  PROPERTIES
  OUTPUT_NAME "oncvpspnr.x"
)

install(
  TARGETS
  oncvpsp oncvpsp_r oncvpsp_nr
  DESTINATION
  "${CMAKE_INSTALL_BINDIR}"
)
